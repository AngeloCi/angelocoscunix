#================================================================
#This is a supporting class for workers.py=======================
#================================================================
import os
import sys


class WorkersSetup:
#===============================================================
#Initial setup (constructor) for a workers total time stamp=====
#===============================================================
      def __init__ (self,name,total):
                       self.name = name
                       self.total = total
#===============================================================
#creats the file that all the names and total times are put into
#===============================================================
      def WorkersTotal(self):
                       workerTotalOut = str('workersTotal.txt')
                       workerTotal = open(workerTotalOut, 'a')
                       workerTotal.write(str(self.name) + "Total:" + " " + str(self.total) + " " + "Minutes " + "\n")
                       workerTotal.close()
                               
#===============================================================
#===============================================================
#===============================================================
                                   
