from workersDocumentText import *

#==================================================
#WELCOME TO THE PAY STUB TOTALER
#Helpful for keeping track of weekly time for 
#employees!
#==================================================
#Instructions
#$python workersMain.py MasterFile.txt(or whatever)
#
#For this program worker files end in .peeps
#==================================================
if len(sys.argv) !=2:
    print("To Run: python workersMain.py workerFiles.txt" )
else:
    print "Using worker data from the file", str(sys.argv[1])
    dataFile = str(sys.argv[1])

workerData = open(dataFile,"r")

print "Reading from a file: " + dataFile
#=================================================
Temp = 0;
workList =[]
FirstLine = 0
PersonTotal = 0
#=================================================
def Total(hours,mins): #figures out the total mins
          h = int(hours.replace('hr',''))
          m = int(mins.replace('mins','')) 
          s = h * 60 + m
          return s
#=================================================
# Strip the first line into a name variable:
for line in workerData.readlines():
    if ((FirstLine == 0) or (FirstLine  % 8 == 0)) :
   # if str(line.count(' ')) == 1:
	fName = str(line)
   #     print "fName = ", fName
    else:
    #    print line
        day, hours, mins = map(str, line.split(' '))
        Temp = Total(hours,mins)
        PersonTotal = Temp + PersonTotal
    FirstLine+=1
    if(FirstLine > 1 and FirstLine % 8 == 0):
    
        resentWorker = WorkersSetup(fName,PersonTotal)
        workList.append(resentWorker)
        PersonTotal = 0

#==================================================
#reads into helper file function WorkersTotal
#takes this and puts it into a file 
#adds insteads of overwriting the file
#==================================================
for i in range(len(workList)):
     workerText = workList[i]
     workerText.WorkersTotal()

