
#include <math.h>
//================================================
//This is to where the shapes for pumpkin are made
//#===============================================

int Circle(int totRow, int totCol, int radius, int pRow, int pCol){
	int InOrOut = 0;
	int centerX = totCol/2;
 	int centerY = totRow/2;
	int dist    = 0;
	
	dist = pow((pow(centerX - pCol,2))+(pow(centerY - pRow,2)),0.5);
	
	if (dist <radius){
		InOrOut = 1;
	} 
	return InOrOut;
}

int InStem(int totRow, int totCol, int radius, int pRow, int pCol){
	int InOrOut = 0;
	int centerX = totCol/2;
	int centerY = totRow/2;
	int stemSize = radius/6 ;
        
	
	if ((pRow >=(centerY - radius - stemSize))&&(pRow <= (centerY - radius))){
		if ((pCol >= (centerX - stemSize/2)) && (pCol <= (centerX + stemSize/2))){
		 InOrOut = 1;
		}
	}
	return InOrOut;
}

int InLEye(int totRow, int totCol, int radius, int pRow, int pCol){
	int InOrOut = 0;
	int centerX = totCol/2;
	int centerY = totRow/2;
	int LeyeSize = radius/5;
	if ((pRow >= (centerY - (radius/2) - LeyeSize)) && (pRow <=(centerY - (radius/2)))){
		if((pCol >= (centerX-(radius/2)-LeyeSize)) && (pCol <= ( centerX - LeyeSize))){
		 InOrOut = 2;
		}
	}
	
        return InOrOut; 
}

int InREye(int totRow, int totCol, int radius, int pRow, int pCol){
	int InOrOut = 0;
	int centerX = totCol/2;
	int centerY = totRow/2;
	int ReyeSize = radius/5;
	if( (pRow>=(centerY - (radius/2) - ReyeSize)) && (pRow <=(centerY -(radius/2)))){
		if((pCol<= (centerX + (radius/2) + ReyeSize))&&(pCol >= (centerX + ReyeSize))){
		 InOrOut = 3;
		}	
	}
	return InOrOut;
}


//Possible Mustaiche
int InMouth(int totRow, int totCol, int radius, int pRow, int pCol){
	int InOrOut = 0;
	int centerX = totCol/2;
	int centerY = totRow/2;
	int mouthSize = radius/8;
	if((pRow>=(centerY - (mouthSize/2)))&&(pRow<=(centerY +( mouthSize/2)))){
		if((pCol >= (centerX - mouthSize/2)) && (pCol <= (centerX + mouthSize/2))){
		 InOrOut = 4; 
		}	
	}
	return InOrOut;

}

int InNose(int totRow, int totCol, int radius, int pRow, int pCol){
        int InOrOut = 0;
        int centerX = totCol/2;
        int centerY = totRow/2;
        int noseSize = radius/2;
        if((pRow<=(centerY + (radius/2) + (noseSize/2))) && (pRow >= (centerY + (noseSize/2)))){
                if((pCol >= (centerX - noseSize/2)) && (pCol <= (centerX + noseSize/2))){
                 InOrOut = 5;
                }
        }
        return InOrOut;

}

