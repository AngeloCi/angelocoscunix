# uses camera to read hex value of continues stream
with PiCamera() as camera:
	camera.resolution = (64,64)
with PiRGBArray(camera, size=(8,8) as stream:
	camera.capture(stream, format='rgb',resize(8,8))
	image=stream.array
