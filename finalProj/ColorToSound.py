import numpy as np
import argparse
import cv2
from Sound import *
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", help = "path to the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])

redLower = [0, 0,30]
redUpper = [0,0,255]

greenLower = [0,30,0]
greenUpper = [0,255,0]

blueLower = [30, 0, 0]
blueUpper = [255, 0, 0]

boundaries = [
    (redLower,redUpper), #red
    (greenLower, greenUpper), #green
    (blueLower, blueUpper) #blue
]
redFlag = False
rlower = np.array(redLower, dtype = "uint8")
rupper = np.array(redUpper, dtype = "uint8")
maybe=cv2.inRange(image,rlower,rupper)
for i in range(len(maybe)):
	for j in range(len(maybe[i])):
		if(maybe[i][j] == 255):
			redFlag = True
			break
	#	print(maybe[i][j])

greenFlag = False
glower = np.array(greenLower,dtype = "uint8")
gupper = np.array(greenUpper,dtype = "uint8")
maybeg=cv2.inRange(image,glower,gupper)
for i in range(len(maybeg)):
        for j in range(len(maybeg[i])):
                if(maybeg[i][j] == 255):
                        greenFlag = True
                        break

blueFlag = False
blower = np.array(blueLower,dtype = "uint8")
bupper = np.array(blueUpper,dtype = "uint8")
maybeb=cv2.inRange(image,blower,bupper)
for i in range(len(maybeb)):
        for j in range(len(maybeb[i])):
                if(maybeb[i][j] == 255):
                        blueFlag = True
                        break


#sineGenerator = sineGenerator(0.5,1.0,440.0)		
if redFlag == True:
	print "we have red"
#v dur freq
	rGenerator = sineGenerator(1.0,3.0,440.0)
else:
	print "we dont have redFlag"
if greenFlag == True:
	print "we have green"
	gGenerator = sineGenerator(1.0,3.0,349.228) 
else:
	print "we dont have green"
if blueFlag == True:
	print "we have blue"
	bGenerator = sineGenerator(1.0,3.0,523.251)
else:
	print "we dont have blue"
