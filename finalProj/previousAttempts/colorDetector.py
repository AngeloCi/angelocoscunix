import numpy as np
import argparse
import cv2

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", help = "path to the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])

redLower = [17, 15,100]
redUpper = [50,56,200]

greenLower = [86,31,4]
greenUpper = [220,88,50]

blueLower = [25, 146, 190]
blueUpper = [62, 174, 250]

boundaries = [
    (redLower,redUpper), #red
    (greenLower, greenUpper), #green
    (blueLower, blueUpper) #blue
]


for (lower, upper) in boundaries:
    
    lower = np.array(lower, dtype = "uint8")
    upper = np.array(upper, dtype = "uint8")
    
    rlower = np.array(redLower, dtype = "uint8")
    rupper = np.array(redUpper, dtype = "uint8")
    
    glower = np.array(greenLower, dtype = "uint8")
    gupper = np.array(greenUpper, dtype = "uint8")
    
    blower = np.array(blueLower, dtype = "uint8")
    bupper = np.array(blueUpper, dtype = "uint8")
    
    mask = cv2.inRange(image, lower, upper)
    
    
    maskr = cv2.inRange(image, rlower, rupper)
    maskb = cv2.inRange(image, blower, bupper)
    maskg = cv2.inRange(image, glower, gupper)
    
    
    redFlag = False
    blueFlag = False
    greenFlag = False 
    
    if np.any(maskr, 255):
        redFlag = True
    if np.any(maskb, 255):
        blueFlag = True
    if np.any(maskg, 255):
        greenFlag = True
    
    """
    for i in maskr:
        if i == 255:
            redFlag = True
    for i in maskb:
        if i == 255:
            blueFlag = True
    for i in maskg:
        if i == 255:
            greenFlag = True
    """        
    if blueFlag == True:
        print ("blue")
    else:
        print ("no blue")
    if redFlag == True:
        print ("red")
    else:
        print ("no red")
    if greenFlag == True:
        print ("green")
    else:
        print ("no green")
    
    output = cv2.bitwise_and(image, image, mask = mask)
    
    
    cv2.imshow("images", np.hstack([image, output]))
    cv2.waitKey(0)
    


