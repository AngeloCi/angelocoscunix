# Source: https://stackoverflow.com/questions/8299303/generating-sine-wave-sound-in-python

import pyaudio
import numpy as np

class sineGenerator:
    def __init__(self, volume, duration, f):
        p = pyaudio.PyAudio()

        self.volume = volume        # range [0.0, 1.0]
        fs = 44100                  # sampling rate, Hz, must be integer
        self.duration = duration    # in seconds, may be float
        self.f = f                  # sine frequency, Hz, may be float

        # generate samples, note conversion to float32 array
        samples = (np.sin(2*np.pi*np.arange(fs*duration)*f/fs)).astype(np.float32).tobytes()

        # for paFloat32 sample values must be in range [-1.0, 1.0]
        stream = p.open(format=pyaudio.paFloat32,
                        channels=1,
                        rate=fs,
                        output=True)

        # play. May repeat with different volume values (if done interactively) 
        stream.write(samples)

        stream.stop_stream()
        stream.close()

        p.terminate()




