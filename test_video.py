from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import cv2

camera = PiCamera()
camera.resolution = (640, 480)
camera.framerate = 32
rawCapture = PiRGBArray(camera, size=(640, 480))

time.sleep(0.1)

for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
    
    image = frame.array
    
    cv2.imshow("Frame", image)
    key = cv2.waitKey(1) & 0xFF
    rawCapture.truncate(0)
    
    if key == ord("q"):
        break
    

from soundPlayer import SoundPlayer

dev = 0
SoundPlayer.playTone(900, 0.1, True, dev)
SoundPlayer.playTone(800, 0.1, True, dev)
SoundPlayer.playTone(700, 0.1, True, dev)
time.sleep(1)
SoundPlayer.playTone([900, 800, 600], 5, True, dev)
print ("done")