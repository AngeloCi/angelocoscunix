#!/bin/bash
for f in $(find . -name "*.tex" -type f)
do
   pdflatex $f
done

for f in $(find . -name ".log" -type f)
do 
   rm $f
done

for f in $(find . -name ".aux" -type f)
do
 rm $f
done

for f in $(find . -name ".bib" -type f)
do
 rm $f
done  
