/* Header File */
/* List of all my functions in myShapes.c */
int Circle(int tRow, int tCol, int rad, int pRow, int pCol);
int InStem(int tRow, int tCol, int rad, int pRow, int pCol);
int InLEye(int tRow, int tcol, int rad, int pRow, int pCol);
int InREye(int tRow, int tcol, int rad, int pRow, int pCol);
int InMouth(int tRow, int tCol, int rad, int pRow, int pCol);
int InNose(int tRow, int tCol, int rad, int pRow, int pCol);
