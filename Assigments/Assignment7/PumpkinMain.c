#include <stdio.h>
#include <stdlib.h>
#include "MyFunctions.h"

/*
 * This program is a c program to create a pumpkin with YOUR specified perameters
 * Pumpkin OutFileName numRows numCOls radius
 * The Rows and columns for the size of the image to produce.
*/

int main(int argc, char *argv[]){
	int numRows;
	int numCols;
	int imageSize;
	int row, col;
	int radius;
	int InOut;
	int InOutTwo;
	int InOutThree;
	int InOutFour;
        int InOutFive;
	int InOutSix;
	unsigned char *outImage;
	unsigned char *ptr;
	FILE *outputFP;

	printf("==================================\n");
        printf("Its the great pumpkin Charly Brown\n");
        printf("==================================\n\n");

	if(argc !=5){
		printf("Usage./TheGreatPump OUTfileName numrow numcols radius \n");
		exit(1);
	}
	if((numRows = atoi(argv[2])) <= 0){
        	printf("Error: Number of rows needs to be positive");
	}
	if((numCols = atoi(argv[3])) <= 0){
		printf("Error: Number of columns needs to be positive");
	}	
	if((radius = atoi(argv[4])) <= 0){
		printf("Error: Number of columns needs to be positive");
	}
	// =====================================
        // Set the space up for the great pumkin
        // =====================================

	imageSize = numRows*numCols*3;
	outImage  = (unsigned char *) malloc(imageSize); //get enough space for image
	/* Open a file to put output image into */
	if((outputFP = fopen(argv[1],"w")) == NULL){
		perror("output open error");
		printf("Error: can not open output file\n");
		exit(1);
	}  

	//=======================
	//Where the magic happens
        //=======================
	
	ptr = outImage;
	
	for(row = 0; row < numRows; row++){
	   for(col =0; col < numCols; col++){
	      //Walk through each row of the image column by column.
	      //Use a function to decide if you are in the circle or not.
	   	InOut = Circle(numRows,numCols,radius,row, col);
		InOutTwo = InStem(numRows, numCols,radius,row, col);
		InOutThree = InLEye(numRows, numCols, radius, row, col);
		InOutFour = InREye(numRows, numCols, radius, row, col);
	        InOutFive = InMouth(numRows, numCols, radius, row, col);
		InOutSix  = InNose(numRows, numCols, radius, row, col);
		if(InOut == 1){
		// Orange pixel
	 	 *ptr    = 255;
		 *(ptr+1)= 51;
		 *(ptr+2)= 0;
		}
		else{
		// white
		 *ptr = 255;
		 *(ptr+1) = 255;
		 *(ptr+2) = 255;
		}
		if(InOutTwo == 1){
		 *ptr = 200;
		 *(ptr+1) = 220;
		 *(ptr+2) = 100;
		}
		if(InOutThree == 2){
		 *ptr = 1;
		 *(ptr + 1) = 1;
		 *(ptr + 2) = 1;
		}
		if(InOutFour == 3){
		 *ptr = 1;
		 *(ptr + 1) = 1;
		 *(ptr + 2) = 1;
		}
                if(InOutFive == 4){
		 *ptr =1;
		 *(ptr + 1) = 1;
		 *(ptr + 2) = 1;
		}
		if(InOutSix == 5){
		 *ptr = 1;
		 *(ptr+1) = 1;
		 *(ptr+2) = 1;
		}
		ptr += 3;
	   }

	}
	ptr = outImage;

        
	// Put all of this info into a file with a header.
        fprintf(outputFP, "P6 %d %d 255\n", numCols, numRows);
	fwrite(outImage, 1, imageSize, outputFP);
	
	/* Done */
	fclose(outputFP);
	return 0;
}

















